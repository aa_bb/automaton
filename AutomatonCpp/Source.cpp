#include "Header.h"
using namespace std;

uint16_t crc16(const uint8_t *buf, int len)
{
	register int counter;
	register uint16_t crc = 0;
	for (counter = 0; counter < len; counter++)
		crc = (crc << 8) ^ crc16tab[((crc >> 8) ^ *(char *)buf++) & 0x00FF];
	return crc;
}

void headersPrint(ofstream& primary, ofstream& signals) {
	primary << setw(width) << "Ax" << setw(width) << "Ay" << setw(width) << "Az" <<
		setw(width) << "Wx" << setw(width) << "Wy" << setw(width) << "Wz" <<
		setw(width) << "Tax" << setw(width) << "Tay" << setw(width) << "Taz" <<
		setw(width) << "Twx" << setw(width) << "Twy" << setw(width) << "Twz" <<
		setw(width) << "S" << setw(width) << "Timestamp" << setw(width) <<
		"Status" << setw(width) << "Number" << endl;
	signals << setw(width) << "Signal" << endl;
}

void outPrimary(PrimaryData primaryData, ofstream& out) {
	out << setw(width) << primaryData.Ax << setw(width) << primaryData.Ay << setw(width) << primaryData.Az <<
		setw(width) << primaryData.Wx << setw(width) << primaryData.Wy << setw(width) << primaryData.Wz <<
		setw(width) << primaryData.Tax << setw(width) << primaryData.Tay << setw(width) << primaryData.Taz <<
		setw(width) << primaryData.Twx << setw(width) << primaryData.Twy << setw(width) << primaryData.Twz <<
		setw(width) << primaryData.S << setw(width) << primaryData.Timestamp << setw(width) <<
		static_cast<int>(primaryData.Status) << setw(width) << static_cast<int>(primaryData.Number) << endl;
}
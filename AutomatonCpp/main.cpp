#include "Header.h"

int main() {
	uint8_t input;
	int length = 0;
	int count, countGood, countCrash;
	count = countGood = countCrash = 0;
	states mode = Sync1;
	PrimaryData primaryData;
	Signal signal;

	ifstream in(inputPath, ios::binary);
	ofstream primary("primary.txt");
	ofstream signals("signals.txt");
	headersPrint(primary, signals);

	if (!in.is_open()) {
		primary << "Input file not opened";
		signals << "Input file not opened";
		signals.close();
		primary.close();
		return 0;
	}

	cout << "Programm is run...";
	while (!in.eof()) {
 		in.read(reinterpret_cast<char*>(&input), sizeof(input));
		switch (mode) {
		case Sync1:
			if (input == 0xAA)
				mode = Sync2;
			break;
		case Sync2:
			if (input == 0xAA)
				mode = ReadLength;
			else
				mode = Sync1;
			break;
		case ReadLength:
			if (input == 0xAA)
				mode = ReadLength;
			else {
				length = input;
				mode = ReadPackage;
			}
			break;
		case ReadPackage:
			if (input == 0x87) {
				count++;
				in.read(reinterpret_cast<char*>(&primaryData), 42);
				uint16_t crc, crc2;
				in.read(reinterpret_cast<char*>(&crc), sizeof(uint16_t));
				uint8_t* buf = new uint8_t[43];
				memcpy(buf, &input, sizeof(input));
				memcpy(buf + 1, &primaryData, 42);
				for (int i = 0; i < 43; i++) {
					if (buf[i] == 0xAA)
					{
						in.seekg(-(45 - i), ios_base::cur);
						break;
					}
				}
				crc2 = crc16(buf, 43);
				if (crc == crc2) {
					outPrimary(primaryData, primary);
					countGood++;
				}
				else
					countCrash++;
				mode = Sync1;
			}
			else if (input == 0x98) {
				in.read(reinterpret_cast<char*>(&signal), sizeof(Signal));
				if (signal.signal == 0xAA) {
					in.seekg(-1, ios_base::cur);
					break;
				}
				if (length == 2)
					signals << setw(width) << static_cast<int>(signal.signal) << endl;
				mode = Sync1;
			}	
			else if (input == 0xAA)
				mode = Sync2;
			else mode = Sync1;
			break;
		}
	}
	primary << "Good/Crashed/All" << endl << countGood << "/" << countCrash << "/" << count;
	signals.close();
	primary.close();
	in.close();
	return 0;
}